import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
import dateFormat from 'dateformat';



function Marketplace() {
  const [ allListings, setAllListings ] = useState([]);
  const [ listingCategory, setListingCategory ] = useState("Category");

  useEffect( () => {
    axios.post("http://localhost:3001/marketplace/").then( (res) => {
      setAllListings(res.data.allListings);}
    )
  }, []) 

  const becomeVerifiedSeller = (e) => {
    e.preventDefault()
    alert("Congratulations.")
  }

  const filteredByCategories = allListings.filter((listing) => {
    if (listingCategory === "Category") { return true }
    else { return(listing.listingCategory === listingCategory) }
  })

  return (
    <>
    <div className="marketplaceMenu">
      <div className="marketplaceMenuFilters">
        <div className={listingCategory==="Category"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Category")}}>All categories</div>
        <div className={listingCategory==="Electronics"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Electronics")}}>Electronics</div>
        <div className={listingCategory==="Automotive"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Automotive")}}>Automotive</div>
        <div className={listingCategory==="Health and household"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Health and household")}}>Health and household</div>
        <div className={listingCategory==="Home and kitchen"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Home and kitchen")}}>Home and kitchen</div>
        <div className={listingCategory==="Pet supplies"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Pet supplies")}}>Pet supplies</div>
        <div className={listingCategory==="Sports and outdors"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Sports and outdors")}}>Sports and outdors</div>
        <div className={listingCategory==="Tools"? "chosenCategory":"notChosenCategory"} onClick={()=>{setListingCategory("Tools")}}>Tools</div>
      </div>
      <div className="marketplaceCreateListingLink"><Link to="createListing" className="marketplaceMenuLink">List an item</Link></div>
      <div className="becomeVerifiedSeller" onClick={becomeVerifiedSeller}>Become a verified seller</div>
    </div>

    
    <div className="marketplaceContent">
      {filteredByCategories
      .sort((a, b) => a.updatedAt > b.updatedAt ? -1 : 1)
      .map((listing, key)=>{return(
        <div className="listing" key={key}>
          <div className="userData">
            <img className="profilePic" src={`/uploadedImages/${listing.User.profilePic}`} alt=""/>
            <Link to={`/profile/${listing.User.id}`} className="name">{listing.User.name}</Link> 
          </div> 
          <div className="date">{ dateFormat(listing.updatedAt, "dddd, dS 'of' mmmm, yyyy 'at' H:MM:ss") }</div>
          <div className="listingText">{ listing.listingText }</div>
          <div className="listingPrice">{ listing.listingPrice } €</div>
          <img className="listingImage" src={`/uploadedImages/${listing.listingImage}`} alt="Listing"/>
          {listing.listingCategory !== "Category" && <div className="listingCategory">{ listing.listingCategory }</div>}
        </div>
      )})}  
    </div>
    </>
  );
}

export default Marketplace;
