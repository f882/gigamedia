import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
import dateFormat from 'dateformat';


function FindPeople() {
  const [ allStatuses, setAllStatuses ] = useState([]);
  const [ statusCategory, setStatusCategory ] = useState("Category");

  useEffect( () => {
    axios.post("http://localhost:3001/findPeople/").then( (res) => {
      setAllStatuses(res.data.allStatuses);}
    )
  }, []) 

  const filteredByCategories = allStatuses.filter((status) => {
    if (statusCategory === "Category") { return true }
    else { return(status.statusCategory === statusCategory) }
  })

  return (
    <>
      <div className="findPeopleMenu">
        <div className="findPeopleMenuFilters">
          <div className={statusCategory==="Category"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Category")}}>All categories</div>
          <div className={statusCategory==="Sport"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Sport")}}>Sport</div>
          <div className={statusCategory==="Education"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Education")}}>Education</div>
          <div className={statusCategory==="Gaming"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Gaming")}}>Gaming</div>
          <div className={statusCategory==="Dating"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Dating")}}>Dating</div>
          <div className={statusCategory==="Betting"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Betting")}}>Betting</div>
          <div className={statusCategory==="Meetup"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Meetup")}}>Meetup</div>
          <div className={statusCategory==="Other"? "chosenCategory":"notChosenCategory"} onClick={()=>{setStatusCategory("Other")}}>Other</div>
        </div>
        <div className="findPeopleCreateStatusLink"><Link to="createStatus" className="findPeopleMenuLink">Create a status</Link></div>
      </div>
      
      <div className="findPeopleContent">
        {filteredByCategories  
          .sort((a, b) => a.updatedAt > b.updatedAt ? -1 : 1)
          .map((status, key)=>{return(
          <div className="status" key={key}>
            <div className="userData">
              <img className="profilePic" src={`/uploadedImages/${status.User.profilePic}`} alt=""/>
              <Link to={`/profile/${status.User.id}`} className="name">{status.User.name}</Link> 
            </div>
            <div className="date">{ dateFormat(status.updatedAt, "dddd, dS 'of' mmmm, yyyy 'at' H:MM:ss") }</div>
            <div className="statusText">{ status.statusText }</div>
          </div>
        )})}  
      </div>
    </>
  );
}

export default FindPeople;

