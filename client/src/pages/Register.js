import React from 'react';
import ImageUploading from 'react-images-uploading';
import { useState } from "react";
import axios from "axios";
import { Link, useNavigate } from 'react-router-dom';

function Register() {
  const [ username, setUsername ] = useState(""); 
  const [ password, setPassword] = useState("");
  const [ email, setEmail ] = useState("");
  const [ name, setName ] = useState("");
  const [ profilePic, setProfilePic ] = useState([]);

  const navigate = useNavigate()

  const register = (e) => {
    e.preventDefault();
    if (profilePic.length !== 0) {
      const formData = new FormData();
      formData.append("username", username);
      formData.append("password", password);
      formData.append("email", email);
      formData.append("name", name);
      formData.append("profilePic", profilePic[0].file);
      axios.post("http://localhost:3001/register/", formData).then(
        (response) => {
          console.log(response);
          navigate("/login");
        }
      )
    } 
    else {
      alert("Naloži sliko.")
    }
  }

  return (
    <>
      <div className="registerContainer">
        <h2 className="registerHeading">Register</h2>
        <form>
          <input className="registerData" type="text" placeholder="Username" onChange={(e)=>setUsername(e.target.value)} /> <br/>
          <input className="registerData" type="password" placeholder="Password" onChange={(e)=>setPassword(e.target.value)} /> <br/>
          <input className="registerData" type="email" placeholder="Email" onChange={(e)=>setEmail(e.target.value)} /> <br/>
          <input className="registerData" type="text" placeholder="Name" onChange={(e)=>setName(e.target.value)} /> <br/>
        </form>

        <ImageUploading value={profilePic} onChange={(imageList, addUpdateIndex) => {setProfilePic(imageList)}} dataURLKey="data_url" >
          {({ imageList, onImageUpload, onImageUpdate, onImageRemove }) => (
            <div>
              
              <div className="handleImageOnRegister" onClick={onImageUpload}> Upload image (only jpg)</div>
              
              {(imageList.length !== 0) && 
                <>
                  <img src={imageList[0]['data_url']} alt="" width="100" />
                  <div className="handleImageOnRegister" onClick={() => onImageUpdate(0)}>Update</div>
                  <div className="handleImageOnRegister" onClick={() => onImageRemove(0)}>Remove</div>
                </>
              }

            </div>
          )}
        </ImageUploading>

        <div className="registerButton" onClick={register}>Register</div>
         
      </div>
      <Link className="loginLink" to="/login">Login</Link>
    </>
  );
}

export default Register;
