import React from 'react';
// https://www.npmjs.com/package/react-images-uploading
import ImageUploading from 'react-images-uploading';
import { useState } from "react";
import axios from "axios";
import { useNavigate } from 'react-router-dom';
// https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects

function CreatePost() {

  const [postText, setPostText] = useState("");
  const [postImage, setPostImage] = useState([]);
  
  const navigate = useNavigate();

  const createPost = (e) => {
    e.preventDefault();
    if (postImage.length !== 0) {
      const formData = new FormData();
      formData.append("postText", postText);
      formData.append("postImage", postImage[0].file);
      formData.append("userId", localStorage.getItem("userId"));
      formData.append("public", document.getElementById('publicCheckbox').checked === true ? 1 : 0);
      axios.post("http://localhost:3001/posts/create", formData).then(navigate("/"));
    } 
    else {
      alert("Naloži sliko. ")
    }
  }

  return (
    <div className="createPostContainer">
      <form>
        <input className="postTextOnCreatePage" type="text" placeholder=" Post text" onChange={(e)=>{setPostText(e.target.value)}} />
      </form>
      <div id="wantPublic">
        <label>
          <input type="checkbox" id="publicCheckbox"/>
          Do you want your post to be public?
        </label>
      </div>
      <ImageUploading value={postImage} onChange={(imageList, addUpdateIndex) => {setPostImage(imageList)}} dataURLKey="data_url" >
        {({ imageList, onImageUpload, onImageUpdate, onImageRemove }) => (
          <div>
            
            <div className="handleImageOnCreatePage" onClick={onImageUpload}> Upload image (only jpg)</div>
            
            {imageList.length !== 0 && 
              <>
                <img className="imgOnCreatePage" src={imageList[0]['data_url']} alt="" />
                <div className="handleImageOnCreatePage" onClick={() => onImageUpdate(0)}>Update</div>
                <div className="handleImageOnCreatePage" onClick={() => onImageRemove(0)}>Remove</div>
              </>
            } 

          </div>
        )}
      </ImageUploading>
      
      <div className="createPostButton" onClick={createPost}>Create post</div>
    </div>
  );
}

export default CreatePost;
