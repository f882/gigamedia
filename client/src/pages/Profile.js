import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import dateFormat from 'dateformat';

// na tej točki se ve da user že obstaja
function Profile() {
  const [ userData, setUserData ] = useState({});
  const [ userPosts, setUserPosts ] = useState([]);
  const [ iAmfollowing, setIAmFollowing ] = useState(false);

  const id = useParams();
  const myId = localStorage.getItem("userId");

  useEffect( () => {
    axios.post("http://localhost:3001/profile/userDataAndPosts", {id: id.id, myId: myId}).then((res) => {
      setUserData(res.data.userData); 
      setUserPosts(res.data.userPosts);
      setIAmFollowing(res.data.iAmfollowing)}
    )
  }, [id, myId]) //myId samo da ne kaže warninga itak se ne more spremnit tekom uporabe

  
  const follow = () => {
    if(iAmfollowing) {
      axios.post("http://localhost:3001/profile/unfollow", {id: id.id, myId: myId}).then( (res) => {
        setIAmFollowing(res.data.iAmfollowing)}
      )
    }
    else {
      axios.post("http://localhost:3001/profile/follow", {id: id.id, myId: myId}).then( (res) => {
        setIAmFollowing(res.data.iAmfollowing)}
      )
    }
  }

  return (
    <>
      <div className="userDataOnProfile">
        <img className="profilePicOnProfile" src={ `/uploadedImages/${userData.profilePic}` } alt="UserImage"/>
        <div className="nameOnProfile">{userData.name}</div>
        {( myId !== id.id && !iAmfollowing ) && ( <div className="follow" onClick={follow}>Follow</div> )}
        {( myId !== id.id && iAmfollowing ) && ( <div className="follow" onClick={follow}>Unfollow</div> )}
        {( myId !== id.id ) && ( <Link to={`/chat/${id.id}`}><div className="chatButtonOnProfile" >Chat</div></Link> )}
      </div>
      
      {userPosts
      .sort((a, b) => a.updatedAt > b.updatedAt ? -1 : 1)
      .map((post, key)=>{return(
        <div className="post" key={ key }>
          <div className="userData">
            <img className="profilePic" src={ `/uploadedImages/${userData.profilePic}` } alt="UserImage"/>
            <span className="name">{userData.name}</span> 
          </div>
          <div className="date">{ dateFormat(post.updatedAt, "dddd, dS 'of' mmmm, yyyy 'at' H:MM:ss") }</div>
          <div className="postText">{ post.postText }</div>
          <img className="postImage" src={ `/uploadedImages/${post.postImage}` } alt="Post"/>
        </div>
      )})}
    </>
  );
}
 
export default Profile; 



 

 
