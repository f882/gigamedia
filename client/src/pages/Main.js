import React, { useEffect, useState } from 'react';
import { Outlet, Link, useNavigate } from "react-router-dom";
import axios from "axios";

function Main({socket}) {
  const myId = localStorage.getItem("userId")


  const navigate = useNavigate();

  // prestreze ce ni logiran
  useEffect( () => {
    if(!myId) {
      navigate("/login");
    }else{
      if (!socket.connected) {
        console.log("connected")
        let username = localStorage.getItem("name");
        let id = localStorage.getItem("userId");
        let img = localStorage.getItem("profilePic");
        socket.auth = { username,id,img };
        socket.connect();
      }
      
    }
  })

  const logout = () => {
    localStorage.clear();
    socket.disconnect()
    navigate("/");
  }

  const [ nameForSearch, setNameForSearch ] = useState("");

  const searchProfile = (e) => {
    e.preventDefault();
    
    if(nameForSearch !== "") {
      axios.post("http://localhost:3001/profile/userExists", {nameForSearch: nameForSearch}).then(
        (response) => {
          if (response.data.error) { 
            alert(response.data.error)
          }
          else{
            setNameForSearch("");
            navigate(`/profile/${response.data.id}`);
          }
        }
      )
    }
  }

  return (
    <>
      <div className="mainMenu">
        <div className="userDataInMainMenu">
          <img className="profilePicInMainMenu" src={`/uploadedImages/${localStorage.getItem("profilePic")}`} alt="ProfilePicture"></img>
          <Link to={`/profile/${myId}`} className="nameInMainMenu"> {localStorage.getItem("name")} </Link> <br />            
        </div> 
          
        <input className="searchBar" type="text" placeholder=" Search for users" onChange={(e)=>{setNameForSearch(e.target.value)}} value={nameForSearch} ></input>
        <div className="searchButton" onClick={searchProfile}>Search</div>

        <div className="linksInMainMenu">
          <Link to="/" className="mainMenuLink">All public posts</Link> <br />
          <Link to="friendsPosts" className="mainMenuLink">Your friends posts</Link> <br />
          <Link to="createPost" className="mainMenuLink">Create a post</Link> <br />
          <Link to="marketplace" className="mainMenuLink">Marketplace</Link> <br />
          <Link to="findPeople" className="mainMenuLink">Find people</Link> <br />
          <Link to={`chat`} className="mainMenuLink">Chat</Link> <br />
        </div>
        <div className="logout" onClick={logout}>Logout</div>
      </div>

      <div className="mainContent">
        <Outlet />
      </div>
    </>
  )
}

export default Main;
