import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useState } from "react";
import axios from "axios";

function Login({socket}) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const onLogin=(username,id,img)=>{
    console.log(username)
    socket.auth = { username,id,img };
    socket.connect();
  }

  const login = (e) => {
    e.preventDefault();

    const data = {providedUsername: username, providedPassword: password}

    
    axios.post("http://localhost:3001/login/", data).then( (response) => {
      if (response.data.error) { 
        alert(response.data.error)
      }
      else{
        localStorage.setItem("userId", response.data.id);
        localStorage.setItem("username", response.data.username);
        localStorage.setItem("profilePic", response.data.profilePic);
        localStorage.setItem("name", response.data.name);
        onLogin(localStorage.getItem("name"),localStorage.getItem("userId"),localStorage.getItem("profilePic"))
        navigate("/");
      }
    });
  }

  return (
    <>
      <div className="loginContainer">
        <h2 className="loginHeading">Login</h2>
        <input className="loginData" type="text" placeholder="Username" onChange={(event) => {setUsername(event.target.value);}} /> <br/>
        <input className="loginData" type="password" placeholder="Password" onChange={(event) => {setPassword(event.target.value);}} /> <br/>
        <div className="loginButton" onClick={login}> Login </div>
      </div>
      <Link className="registerLink" to="/register">Register</Link>
    </>
  );
}

export default Login;
