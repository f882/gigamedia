import React, { useState } from 'react';
import axios from "axios";
import { useNavigate } from 'react-router-dom';

function CreateStatus() {
  const [ statusText, setStatusText ] = useState("");
  const [ statusCategory, setStatusCategory ] = useState("Category");

  const navigate = useNavigate();

  const createStatus = (e) => {
    const userId = localStorage.getItem("userId");
    const data = {statusText: statusText, statusCategory: statusCategory, userId: userId};
    axios.post("http://localhost:3001/findPeople/createStatus", data).then(() => {
      navigate("/findPeople")}
    )
  }

  return (
    <div className="createStatusContainer">
      <input className="statusTextOnCreateStatusPage" type="text" placeholder="Status text" onChange={(e)=>{setStatusText(e.target.value)}} />
      
      <div className="dropdown">
        <span>{statusCategory}</span>
        <div className="dropdown-content">
          <div className="category" onClick={()=>{setStatusCategory("Sport")}}>Sport</div>
          <div className="category" onClick={()=>{setStatusCategory("Education")}}>Education</div>
          <div className="category" onClick={()=>{setStatusCategory("Gaming")}}>Gaming</div>
          <div className="category" onClick={()=>{setStatusCategory("Dating")}}>Dating</div>
          <div className="category" onClick={()=>{setStatusCategory("Betting")}}>Betting</div>
          <div className="category" onClick={()=>{setStatusCategory("Meetup")}}>Meetup</div>
          <div className="category" onClick={()=>{setStatusCategory("Other")}}>Other</div>
        </div>
      </div>
      
      <div className="createStatusButton" onClick={createStatus}>Create status</div>
    </div>
  );
}

export default CreateStatus;