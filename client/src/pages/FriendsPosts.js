import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom"
import axios from 'axios';
import dateFormat from 'dateformat';



function FriendsPosts() {
  const [ perosnalizedPosts, setPerosnalizedPosts ] = useState([]);
  
  const myId = localStorage.getItem("userId");

  useEffect( () => {
    axios.post("http://localhost:3001/posts/personalized", {UserId: myId}).then( (res) => {
      setPerosnalizedPosts(res.data.personalizedPosts);}
    )
  }, [myId]) 

  return (
    <>
      {perosnalizedPosts
      .sort((a, b) => a.updatedAt > b.updatedAt ? -1 : 1)
      .map((post, key)=>{return(
        <div className="post" key={key}>
          <div className="userData">
            <img className="profilePic" src={`/uploadedImages/${post.User.profilePic}`} alt=""/>
            <Link to={`/profile/${post.User.id}`} className="name">{post.User.name}</Link> 
          </div> 
          <div className="date">{ dateFormat(post.updatedAt, "dddd, dS 'of' mmmm, yyyy 'at' H:MM:ss") }</div>
          <div className="postText">{ post.postText }</div>
          
          <img className="postImage" src={`/uploadedImages/${post.postImage}`} alt="Post"/>
        </div>
      )})}
    </>
  )
}
//format(new Date(post.updatedAt), 'yyyy/MM/dd kk:mm:ss')

export default FriendsPosts
