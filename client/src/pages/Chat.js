import axios from 'axios'
import React, { useEffect, useState, useRef } from 'react'
import { Link, useParams } from 'react-router-dom'

function Chat({socket}) {
  const [ usersICanChatTo, setUsersToChatTo ] = useState([]);
  const [ chatToRender, setChatToRender ] = useState([]);
  const [ messageToSend, setMessageToSend ] = useState("");
  const [ currentChatId, setCurrentChatId ] = useState("");
  const [ users, setUsers ] = useState([]);

  const chattingWithUser = useParams();
  const chattingWithId = chattingWithUser.userId;
  const myId = localStorage.getItem("userId");

  useEffect(()=>{
    socket.on("users", (users) => {
      setUsers(users);
    })

    socket.on("user connected", (user) => {
      console.log(`${user.userId} connected`)
      setUsers(content => [...content,user])
    });
    
    socket.on("user disconnected", (user) => {
      setUsers(users.splice(users.indexOf(user),1));
    })

    socket.on("private message", ({ message, userId }) => {
      setChatToRender(current => [...current,{message:message,userId:userId}])
    });

  },[socket]);

  useEffect(()=>{
    socket.emit("need online peers")
    socket.on("online peers", (users) => {
      setUsers(users);
    })
  },[])

  const sendMessage = (e) => {
    e.preventDefault();
    if ((messageToSend !== "") && !!chattingWithId) {
      const data = { message: messageToSend, senderId: myId, chatId: currentChatId }
      let sid = 0;
      if (users.find(e => e.userId == chattingWithId)) {
        sid = users.find(e => e.userId === chattingWithId).socketId;
      }
      socket.emit("private message", {
        content: messageToSend,
        to: sid,
      });
      axios.post("http://localhost:3002/chat/sendMessage", data).then( (res) => {
          setChatToRender(chatToRender => [...chatToRender,data])
          setMessageToSend("");})
    }
  }

  

  // pogovor za prikaz, če pogovor še ne obstaja ga ustvari, npr. ko pride iz market place ali iz find people
  useEffect(() => {
    async function fetchData() {
      if (!!chattingWithId) {
        const data = { myId : myId, chattingTo: chattingWithId}
        const res = await axios.post("http://localhost:3002/chat/messages", data);
        setCurrentChatId(res.data.ChatId);
        setChatToRender(res.data.chatMessages);
      }
        const res = await axios.post("http://localhost:3002/chat/", { myId: myId});
        setUsersToChatTo(res.data.usersICanChatTo);
    }
    fetchData();

  }, [myId, chattingWithId]);





  // https://stackoverflow.com/questions/72372407/react-auto-scroll-to-bottom-of-a-div
  const messagesEndRef = useRef(null)
  useEffect(() => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
  }, [chatToRender]);

  
  return (
    <>
    <div className="chatMenu">
    {usersICanChatTo.map((user, key)=>{return(!(user.id == myId) ?
        <div className="userDataInChatMenu" key={key}>
          <img className="profilePicInChatMenu" src={`/uploadedImages/${user.profilePic}`} alt=""/>
          <Link to={`/chat/${user.id}`} className="nameInChatMenu">{user.name}</Link>
          <span className='userAvailability'>{users.some(e => e.userId === (user.id).toString()) ? "🟢 Online" : "⚪ Offline"}</span>
        </div> : void(0)
      )})}
    </div> 

    <div className="chatContent">
      <div className="renderedChat" >
        {
          !chattingWithId ? 
          "Choose a person to start chatting with." 
          :
          chatToRender.slice(-15).map((message, key)=>{ return( 
            <div className="message" key={key}>
              <div 
                className={message.senderId===myId?"myMessage":"notMyMessage"} 
                key={key}
                > {message.message}
              </div>
            </div>
          )})
        }

        <div ref={messagesEndRef} />

      </div>
      
      <div className="messageInputArea">
        <textarea className="messageInput" type="text" value={messageToSend} onChange={(e)=>{setMessageToSend(e.target.value)}} />
        <div className="sendMessageButton" onClick={sendMessage}>Send message</div>
      </div>

    </div>
    </>
  )
}

export default Chat
