import React, { useState } from 'react';
import ImageUploading from 'react-images-uploading';
import axios from "axios";
import { useNavigate } from 'react-router-dom';

function CreateListing() {
  const [ listingText, setListingText ] = useState("");
  const [ listingPrice, setListingPrice ] = useState("");
  const [ listingImage, setListingImage ] = useState([]);
  const [ listingCategory, setListingCategory ] = useState("Category");

  const navigate = useNavigate();

  const createListing = (e) => {
    e.preventDefault();
    if (listingImage.length !== 0) {
      const formData = new FormData();
      formData.append("listingText", listingText);
      formData.append("listingPrice", listingPrice);
      formData.append("listingImage", listingImage[0].file);
      formData.append("listingCategory", listingCategory);
      formData.append("userId", localStorage.getItem("userId"));
      axios.post("http://localhost:3001/marketplace/createListing", formData).then(() => {
        navigate("/marketplace")}
      )    
    } 
    else {
      alert("Naloži sliko. ")
    }
  }

  return (
    <div className="createListingContainer">
      
      <textarea className="listingTextOnCreateListingPage" type="text" placeholder="Item description" onChange={(e)=>{setListingText(e.target.value)}} />
      <input className="itemPriceOnCreateListingPage" type="text" placeholder="Price in €" onChange={(e)=>{setListingPrice(e.target.value)}} />
      
      <div class="dropdown">
        <span>{listingCategory}</span>
        <div className="dropdown-content">
          <div className="category" onClick={()=>{setListingCategory("Category")}}>Category</div>
          <div className="category" onClick={()=>{setListingCategory("Electronics")}}>Electronics</div>
          <div className="category" onClick={()=>{setListingCategory("Automotive")}}>Automotive</div>
          <div className="category" onClick={()=>{setListingCategory("Health and household")}}>Health and household</div>
          <div className="category" onClick={()=>{setListingCategory("Home and kitchen")}}>Home and kitchen</div>
          <div className="category" onClick={()=>{setListingCategory("Pet supplies")}}>Pet supplies</div>
          <div className="category" onClick={()=>{setListingCategory("Sports and outdors")}}>Sports and outdors</div>
          <div className="category" onClick={()=>{setListingCategory("Tools")}}>Tools</div>
        </div>
      </div>

      <ImageUploading value={listingImage} onChange={(imageList, addUpdateIndex) => {setListingImage(imageList)}} dataURLKey="data_url" >
        {({ imageList, onImageUpload, onImageUpdate, onImageRemove }) => (
          <div>
            
            <div className="handleImageOnCreateListing" onClick={onImageUpload}> Upload image (only jpg)</div>
            
            {imageList.length !== 0 && 
              <>
                <img className="imgOnCreateListing" src={imageList[0]['data_url']} alt="" />
                <div className="handleImageOnCreateListing" onClick={() => onImageUpdate(0)}>Update</div>
                <div className="handleImageOnCreateListing" onClick={() => onImageRemove(0)}>Remove</div>
              </>
            } 

          </div>
        )}
      </ImageUploading>
      
      <div className="createListingButton" onClick={createListing}>List item</div>
    </div>
  );
}

export default CreateListing;
