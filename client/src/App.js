import './App.css';
import socket from './socket'
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import Main from './pages/Main';
import AllPosts from './pages/AllPosts';
import FriendsPosts from './pages/FriendsPosts';
import CreatePost from './pages/CreatePost';
import Profile from './pages/Profile';
import Login from './pages/Login';
import Register from './pages/Register';
import Marketplace from './pages/Marketplace';
import FindPeople from './pages/FindPeople';
import CreateListing from './pages/CreateListing';
import CreateStatus from './pages/CreateStatus';
import Chat from "./pages/Chat";




function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={ <Main socket={socket}/> }>
          <Route index element={ <AllPosts /> } />
          <Route path="friendsPosts" element={ <FriendsPosts />} />
          <Route path="createPost" element={ <CreatePost /> } />
          <Route path="profile/:id" element={ <Profile /> } />  
          <Route path="marketplace" element={ <Marketplace /> } />
          <Route path="marketplace/createListing" element={ <CreateListing /> } />
          <Route path="findPeople" element={ <FindPeople /> } /> 
          <Route path="findPeople/createStatus" element={ <CreateStatus /> } />
          <Route path="chat/:userId" element={ <Chat socket={socket}/> } />
          <Route path="chat" element={ <Chat socket={socket}/> }/>
        </Route>
        <Route path="/login" element={ <Login socket={socket}/> } />
        <Route path="/register" element={ <Register /> } />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
