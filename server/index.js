const express = require('express');
const app = express();
const http = require('http').createServer(app);
const cors = require("cors");
app.use(cors());
const io = require('socket.io')(http,{cors: {origin:"http://localhost:3000"}});
app.use(express.json());

const loginRouter = require('./routes/Login');
app.use("/login", loginRouter);
const registerRouter = require('./routes/Register');
app.use("/register", registerRouter);
const postsRouter = require('./routes/Posts');
app.use("/posts", postsRouter);
const profileRouter = require('./routes/Profile');
app.use("/profile", profileRouter);
const marketplaceRouter = require('./routes/Marketplace');
app.use("/marketplace", marketplaceRouter);
const findPeopleRouter = require('./routes/FindPeople');
app.use("/findPeople", findPeopleRouter);
const chatRouter = require('./routes/Chat');
app.use("/chat", chatRouter);

const db = require('./models');

db.sequelize.sync().then( () => {
  app.listen( 3001 );
} );


http.listen(3002, () => {
  console.log('listening on *:3002');
});



const ct = require("./lib/Chat.js");
const { userInfo } = require('os');


io.use((socket, next) => {
  const username = socket.handshake.auth.username;
  const identificator = socket.handshake.auth.id;
  const pfpic = socket.handshake.auth.img;
  if (!username) {
    return next(new Error("invalid username"));
  }
  socket.username = username;
  socket.uid = identificator;
  socket.pfpic = pfpic;
  next();
});

const users = [];
io.on("connection", (socket) => {
  console.log(`user ${socket.username} connected with id ${socket.uid}`)
  // fetch existing users
  users.push({
    userId:socket.uid,
    socketId:socket.id,});

  console.log(users)

  socket.emit("users", users);
  // notify existing users
  socket.broadcast.emit("user connected",{
    userId:socket.uid,
    socketId:socket.id,
  });

  // forward the private message to the right recipient
  socket.on("private message", ({ content, to }) => {
    socket.to(to).emit("private message", {
      message:content,
      senderId:socket.id,
    });
  });

  // notify users upon disconnection
  socket.on("disconnect", () => {
    socket.broadcast.emit("user disconnected", {
      userId:socket.uid,
      socketId:socket.id,
    });
    var index = users.findIndex(function(item, i){
      return item.userId === socket.uid
    });
    users.splice(index,1)
  });

  socket.on("need online peers", () =>{
    socket.emit("online peers", users)
  })
});
