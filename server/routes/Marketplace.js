const express = require("express");
const router = express.Router();
const { Offers, Users } = require("../models");
const multer  = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '../client/public/uploadedImages')
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + uniqueSuffix +'.jpg')
  }
})

const upload = multer({ storage: storage })

router.post('/createListing', upload.single("listingImage"), async function (req, res) {
  let listingImageData = req.file;
  const listingImage = listingImageData['filename'];

  const listingData = req.body;
  const { listingText, listingPrice, listingCategory, userId } = listingData;

  await Offers.create({
    listingText: listingText,
    listingPrice: listingPrice,
    listingImage: listingImage,
    listingCategory: listingCategory,
    UserId: userId
  });

  res.send("Success.")
});

router.post('/', async (req, res) => {
  const allListings = await Offers.findAll({include: Users});
  res.send({allListings: allListings});
});



module.exports = router;