const express = require("express");
const router = express.Router();
const { Posts, Followings, Users } = require("../models");
const multer  = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '../client/public/uploadedImages')
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + uniqueSuffix +'.jpg')
  }
})

const upload = multer({ storage: storage })

router.post('/create', upload.single("postImage"), async function (req, res) {
  let postImageData = req.file;
  const postImage = postImageData['filename'];

  const postData = req.body;
  const { postText, userId, public } = postData;

  await Posts.create({
    postText: postText,
    postImage: postImage,
    UserId: userId,
    public: public
  });

  res.send("Success.")
});

router.post('/personalized', async (req, res) => {
  const { UserId } = req.body;

  const usersIAmfollowing = await Followings.findAll({ where: {UserId: UserId} });
  
  const usersIAmfollowingId = [];
  usersIAmfollowing.map((value) => { usersIAmfollowingId.push(value.beingFollowed) });
  
  // https://sequelize.org/docs/v6/advanced-association-concepts/eager-loading/
  const personalizedPosts = await Posts.findAll({ where: {UserId: usersIAmfollowingId}, include: [Users] })
  //const personalizedPosts = await Posts.findAll({ where: {UserId: usersIAmfollowingId} })
  
  res.json( {personalizedPosts: personalizedPosts} );
});


router.post('/all', async (req, res) => {
  const posts = await Posts.findAll({ where: {public: 1}, include: [Users] })
  
  res.json( {posts: posts} );
});


module.exports = router;