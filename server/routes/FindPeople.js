const express = require("express");
const router = express.Router();
const { Statuses, Users } = require("../models");


router.post('/', async (req, res) => {
  const allStatuses = await Statuses.findAll({include: Users});
  res.send({allStatuses: allStatuses});
});


router.post('/createStatus', async (req, res) => {
  const statusData = req.body;
  const { statusText, statusCategory, userId } = statusData;
  await Statuses.create({ statusText: statusText, statusCategory: statusCategory, UserId: userId });
  res.send("Success.")
});


module.exports = router;