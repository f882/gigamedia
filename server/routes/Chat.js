const express = require("express");
const router = express.Router();
const { Users, Chats, Messages } = require("../models");
const { Op } = require("sequelize");

router.post('/messages', async (req, res) => {
  const { myId, chattingTo } = req.body;

  const chatData = await Chats.findOne(
    { where: { [Op.or]: [ {user1:myId, user2:chattingTo}, {user1:chattingTo, user2:myId} ] } })
  
  // še ni odprtega pogovora, ga ustvari
  if (!chatData) {
    const chat = await Chats.create( {user1: myId, user2:chattingTo} );
    res.json({chatMessages: [], ChatId: chat.id})
  }
  else {
    const chatMessages = await Messages.findAll({ attributes: ['message','senderId','chatId'], where: {ChatId: chatData.id} })
    res.json({ chatMessages: chatMessages, ChatId: chatData.id });
  }
});

 
router.post('/', async (req, res) => {
  const { myId } = req.body;

  const chatsImHaving1 = await Chats.findAll({attributes: ['user2'],  where: { user1:myId } })
  const chatsImHaving2 = await Chats.findAll({ attributes: ['user1'],  where: { user2:myId } })
  
  const usersICanChatToId = [];
  chatsImHaving1.map((value) => { usersICanChatToId.push(value.user2) });
  chatsImHaving2.map((value) => { usersICanChatToId.push(value.user1) });
  
  const usersICanChatTo = await Users.findAll({ where: {id: usersICanChatToId} })
  
  res.json( {usersICanChatTo:usersICanChatTo} );
});

router.post('/sendMessage', async (req, res) => {
  const { message, senderId, chatId } = req.body;

  await Messages.create({ message: message, senderId:senderId, ChatId:chatId })
  
  res.json("success")
});

module.exports = router;