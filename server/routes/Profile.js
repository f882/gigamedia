const express = require("express");
const router = express.Router();
const { Users, Posts, Followings } = require("../models");

router.post('/userDataAndPosts', async (req, res) => {
  const { id, myId } = req.body;

  const userData = await Users.findOne({ where: { id: id } });
  const userPosts = await Posts.findAll({ where: { UserId: id }});
  const iAmfollowing = await Followings.findOne({ where: {beingFollowed: id, UserId: myId} });
  
  res.json({userData: userData, userPosts: userPosts, iAmfollowing: !!iAmfollowing});
});


router.post("/userExists", async (req, res) => {
  const { nameForSearch } = req.body;

  const user = await Users.findOne({ where: { name: nameForSearch } });

  if (!user) {
    res.json({ error: "User doesn't exist." })
  }
  else {
    res.json({id: user.id});
  }
});


router.post('/follow', async (req, res) => {
  const { id, myId } = req.body;

  await Followings.create({beingFollowed: id, UserId: myId});
  
  res.json( { iAmfollowing: true } );
});


router.post('/unfollow', async (req, res) => {
  const { id, myId } = req.body;

  await Followings.destroy({ where: {beingFollowed: id, UserId: myId} });
  
  res.json( { iAmfollowing: false } );
});






module.exports = router;