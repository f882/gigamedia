const express = require("express");
const router = express.Router();
const { Users } = require("../models");

router.post("/", async (req, res) => {
  const { providedUsername, providedPassword } = req.body;

  const user = await Users.findOne({ where: { username: providedUsername } });

  if (!user) {
    res.json({ error: "User doesn't exist." })
  }
  else if (!(providedPassword == user.password)) {
    res.json({ error: "Wrong username and password combination." });
  }
  else {
    res.json(user);
  }
});



module.exports = router;