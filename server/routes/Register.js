const express = require("express");
const router = express.Router();
const { Users } = require("../models");
const multer  = require('multer');
// zarad koncnice
// https://www.npmjs.com/package/multer?activeTab=readme
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '../client/public/uploadedImages')
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + uniqueSuffix +'.jpg')
  }
})

const upload = multer({ storage: storage })

router.post('/', upload.single("profilePic"), async function (req, res) {
  
  let profilePicData = req.file;
  const profilePic = profilePicData['filename'];

  const userData = req.body;
  const { username, password, email, name } = userData;

  await Users.create({
    username: username,
    password: password,
    email: email,
    name: name,
    profilePic: profilePic
  });

  res.json("User created.");
});

module.exports = router;  