class ChatSession{
    constructor(user1,user2,id,socketId){
        this.user1=user1;
        this.user2=user2;
        this.id=id;
        this.socketId=socketId;
        this.cache=[];
    }

    pushMessage(message) {
        this.cache.push(message)
        if (this.cache.length > 20) {
            this.cache.pop();
        }
    }

}
module.exports = ChatSession;