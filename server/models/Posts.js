module.exports = (sequelize, DataTypes) => {

  const Posts = sequelize.define( "Posts", {
      postText: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      postImage: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      public: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
  } );

  
  Posts.associate = (models) => {
    Posts.belongsTo(models.Users);
  }; 

  return Posts;
}