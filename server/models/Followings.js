module.exports = (sequelize, DataTypes) => {

  const Followings = sequelize.define( "Followings", {
      beingFollowed: { 
        type: DataTypes.STRING, 
        allowNull: false 
      }
  });

  return Followings;
}