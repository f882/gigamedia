module.exports = (sequelize, DataTypes) => {

  const Statuses = sequelize.define( "Statuses", {
      statusText: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      statusCategory: { 
        type: DataTypes.STRING, 
        allowNull: false 
      }
  } );

  Statuses.associate = (models) => {
    Statuses.belongsTo(models.Users);
  }; 
  
  return Statuses;
}