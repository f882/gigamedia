module.exports = (sequelize, DataTypes) => {

  const Chats = sequelize.define( "Chats", {
      user1: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      user2: { 
        type: DataTypes.STRING, 
        allowNull: false 
      }
  }); 

  Chats.associate = (models) => {
    Chats.hasMany(models.Messages, { onDelete: "cascade" });
  }; 

  return Chats;
}