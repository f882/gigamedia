module.exports = (sequelize, DataTypes) => {

  const Offers = sequelize.define( "Offers", {
      listingText: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      listingPrice: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      listingImage: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },
      listingCategory: { 
        type: DataTypes.STRING, 
        allowNull: false 
      },

  } );

  Offers.associate = (models) => {
    Offers.belongsTo(models.Users);
  }; 

  return Offers;
}